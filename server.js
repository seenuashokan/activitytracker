// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var Odoo = require('node-odoo');


//Connect with odoo server
var odoo = new Odoo({
  host: '192.168.1.121',
  port: 8091,
  database: 'goavega_demo',
  username: 'dkashyap@goavega.com',
  password: 'abcd@1234'
});


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



var port = process.env.PORT || 3002;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:3002/api)
router.get('/', function(req, res) {
    res.json({ message: 'Hello!' });
});

// more routes for our API will happen here
//on route that end in /tsEntry
//----------------------------------------------------
router.route('/timesheetentry')

//CREATE TIMESHEET ENTRY IN ODOO (accessed a POST http://localhost:3002/api/timesheetentry)
.post(function(req,res) {

  odoo.connect(function(err) {
  if (err) {
    return console.log(err);
  }
  var date_from = req.body.date;
  var date_to = req.body.date;
  var date = req.body.date;
  var account_id = req.body.account_id;
  var name = req.body.name;
  var unit_amount = req.body.unit_amount;
  var company_id = req.body.company_id;
  var user_id = req.body.user_id;
  var employee_id = parseInt(req.body.employee_id);

  res.json(date_from+','+date_to+','+account_id+','+name+','+unit_amount+','+company_id+','+date+','+user_id+','+employee_id);

    odoo.create('hr_timesheet_sheet.sheet',{'date_from':date_from,'date_to':date_to,'company_id':company_id,'user_id':user_id,'name':name,'employee_id':employee_id},function(err, data) {

    odoo.create('account.analytic.line',{'date':date,'name':name,'account_id':account_id,'unit_amount':unit_amount,'user_id':user_id,'is_timesheet':true},function(err,result) {

    });

});

});

});

router.route('/getProjects')

//GET PROJECT NAMES FROM ODOO (accessed a GET http://localhost:3002/api/getProjects)
.get(function(req,res) {

  odoo.connect(function(err) {
  if (err) {
    return console.log(err);
  }

odoo.search('account.analytic.account',[['company_id','=',1]],function(err, proj) {

odoo.get('account.analytic.account',proj,function(err,projectName) {
  var pronameObj = {};
  for (var i = 0; i < projectName.length; i++) {
      pronameObj.name = projectName[i].name;
      pronameObj.id = projectName[i].id;
  }
  res.status(200).json(pronameObj);
});

});

});

});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================

app.listen(port);

console.log('Server listening on ' + port);
